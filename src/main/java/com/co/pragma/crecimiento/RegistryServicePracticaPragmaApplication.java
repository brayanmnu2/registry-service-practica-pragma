package com.co.pragma.crecimiento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class RegistryServicePracticaPragmaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistryServicePracticaPragmaApplication.class, args);
	}

}
